Title: Corrigindo o erro "Segmentation Fault" na Raspberry Pi
Date: 2021-04-23 09:00
Modified: 2021-04-23 09:00
Category: Raspberry
Tags: raspberrypi, postgresql, docker, container
Slug: erro-segmentation-fault
Authors: Berg Paulo
Status: published


Nessa semana resolvi atualizar os pacotes na minha Raspberry Pi 3B+, onde utilizo a mesma para hospedar alguns bots para o Telegram e também fazer alguns testes com aplicações em ambiente Linux.
Mas ao finalizar o update, me deparo com o seguinte erro ao subir os containers novamente com o Docker:

    :::bash
    startup process (PID 36) was terminated by signal 11: Segmentation fault.

Este erro no caso aconteceu especificamente com o container do PostgreSQL, onde é utilizado por algumas aplicações.

Buscando um pouco sobre o problema, acabei encontrando alguns links relatando usuários com o mesmo empasse, e com uma possível solução para o problema. Um deles foi exatamente em uma issue do repositório [docker-library/postgres](https://github.com/docker-library/postgres/issues/812), onde é recomendado a atualização da biblioteca `libseccomp`.

Pesquisando mais a fundo sobre essa blblioteca acabei descobrindo que a mesma é utilizada por outras bibliotecas para realizar chamadas filtradas no kernel do Linux, onde a mesma também é utilizada pelo Docker.

Seguindo o recomendado na issue do github, aqui alguns passos realizados para a atualização da biblioteca:

## Download do arquivo .deb

Atualmente no reṕositório oficial o pacote disponível ainda se encontra com uma versão antiga("2.3.3-4"). Realizei a verificação do pacote mais recente através do comando:

    :::bash
    $ apt-cache show libseccomp2
    Package: libseccomp2
    Source: libseccomp
    Version: 2.3.3-4
    Architecture: armhf
    Maintainer: Kees Cook <kees@debian.org>
    Installed-Size: 157
    Depends: libc6 (>= 2.4)
    Multi-Arch: same
    Homepage: https://github.com/seccomp/libseccomp
    Priority: optional
    Section: libs
    Filename: pool/main/libs/libseccomp/libseccomp2_2.3.3-4_armhf.deb
    Size: 33052
    SHA256: d5ff36b85b8b6b5ff872a2f90b98a0f84cef24f91d4658d8a717140b75547d9b
    SHA1: 820f21c98ef05b853fecc88e74b14f4d19bba1b2
    MD5sum: 1b3d4f7923f06d4ff53198b29c4a302b
    Description: high level interface to Linux seccomp filter
    This library provides a high level interface to constructing, analyzing
    and installing seccomp filters via a BPF passed to the Linux Kernel`s
    prctl() syscall.
    Description-md5: 7ee97a8161e83bfebc75870eb92bde51

Se fez necessário o download da biblioteca para a arquitetura ARM através do repositório do Debian:

    :::bash
    $ curl http://ftp.debian.org/debian/pool/main/libs/libseccomp/libseccomp2_2.5.1-1_armhf.deb --output libseccomp2_2.5.1-1_armhf.deb

Após isso instalar o pacote com o comando `dpkg`:

    :::bash
    $ sudo dpkg --install libseccomp2_2.5.1-1_armhf.deb

Finalizado a instalação, é possível verificar a versão instalada com o comando `apt-cache show libseccomp2`, obtendo o seguinte retorno:

    :::bash
    $ apt-cache show libseccomp2
    Package: libseccomp2
    Status: install ok installed
    Priority: optional
    Section: libs
    Installed-Size: 129
    Maintainer: Kees Cook <kees@debian.org>
    Architecture: armhf
    Multi-Arch: same
    Source: libseccomp
    Version: 2.5.1-1
    Depends: libc6 (>= 2.4)
    Description: high level interface to Linux seccomp filter
    This library provides a high level interface to constructing, analyzing
    and installing seccomp filters via a BPF passed to the Linux Kernel`s
    prctl() syscall.
    Description-md5: 7ee97a8161e83bfebc75870eb92bde51
    Homepage: https://github.com/seccomp/libseccomp

    Package: libseccomp2
    Source: libseccomp
    Version: 2.3.3-4
    Architecture: armhf
    Maintainer: Kees Cook <kees@debian.org>
    Installed-Size: 157
    Depends: libc6 (>= 2.4)
    Multi-Arch: same
    Homepage: https://github.com/seccomp/libseccomp
    Priority: optional
    Section: libs
    Filename: pool/main/libs/libseccomp/libseccomp2_2.3.3-4_armhf.deb
    Size: 33052
    SHA256: d5ff36b85b8b6b5ff872a2f90b98a0f84cef24f91d4658d8a717140b75547d9b
    SHA1: 820f21c98ef05b853fecc88e74b14f4d19bba1b2
    MD5sum: 1b3d4f7923f06d4ff53198b29c4a302b
    Description: high level interface to Linux seccomp filter
    This library provides a high level interface to constructing, analyzing
    and installing seccomp filters via a BPF passed to the Linux Kernel`s
    prctl() syscall.
    Description-md5: 7ee97a8161e83bfebc75870eb92bde51

Onde a primeiro trecho indica a versão instalada anteriormente, e o segundo trecho, a versão do repositório oficial do Raspbian OS.
Notem no campo "Version", tinhamos a versão "2.3.3-4", e com a instalação manual foi atualizada para a versão "2.5.1-1".

Após a atualização da biblioteca, foi possível subir os containers do PostgreSQL sem mais problemas.