Title: Sua Primeira Aplicação com Flask (versão 2022)
Date: 2022-10-29 09:00
Modified: 2022-10-29 09:00
Category: Flask
Tags: flask, python, microframework
Slug: primeira-aplicacao-flask-2022
Authors: Berg Paulo
Status: published

Estou reescrevendo esse post pois ouveram várias alterações no framework entre as versões __1.1.x__ e a __2.2.x__(último release até a data de postagem).

Você pode dar um conferida mais a fundo acessando o histórico de mudanças: [https://flask.palletsprojects.com/en/2.2.x/changes/](https://flask.palletsprojects.com/en/2.2.x/changes/)

O Flask é um dos microframeworks mais utilizados na linguagem Python, por _"oferecer sugestões e não impõe qualquer dependência ou layout, dando assim liberdade ao desenvolvedor para escolher ferramentas e bibliotecas que desejar"_, além ser simples e prático de inicar uma aplicação como veremos abaixo:

## Iniciando aplicação

Antes de iniciar, certifique-se de que o `python3` e o `pip` estejam instalados em seu SO.

Vamos criar uma pasta para nosso projeto, criar uma `venv` e instalar o `flask`:

Criando uma pasta:

    :::bash
    mkdir primeira_aplicacao_flask

Entrando na pasta:

    :::bash
    cd primeira_aplicacao_flask

Agora vamos criar a nossa `venv`:

    :::bash
    python3 -m venv .venv

Vamos ativar a `venv`:

    :::bash
    source .venv/bin/activate

E instalar `flask` com o `pip`:

    :::bash
    pip3 install flask==2.2.2

Após isso já podemos criar nosso arquivo e iniciar nossa aplicação:

Crie um novo arquivo através do terminal:

    :::bash
    touch app.py

Usando alguma IDE de sua preferência ou o próprio `vim` ou `nano`, abra o arquivo criado no anteriormente e insira o seguinte conteúdo:

    :::python
    from flask import Flask

    def create_app():
        app = Flask(__name__)

        @app.get('/')
        def hello():
          return 'Hello World'

        return app


Devemos exportar as serguintes variáveis de ambiente para o Flask reconheça e inicie a aplicação corretamente:

    :::bash
    export FLASK_DEBUG=true FLASK_APP=app.py

Agora vamos servir nossa aplicação para que possamos ver seu retorno no browser.

    :::bash
    flask run

Checando o terminal, teremos um retorno semelhante a esse:

    :::bash
    * Serving Flask app 'app.py'
    * Debug mode: on
    WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
    * Running on http://127.0.0.1:5000
    Press CTRL+C to quit
    * Restarting with stat
    * Debugger is active!
    * Debugger PIN: 322-628-528

Podemos acessar a aplicação através da url [http://localhost:5000](http://localhost:5000).

Agora você já pode se considerar um desenvolvendor iniciante com micro-framework Flask.<br>
Ainda tem bastante conteúdo sobre Flask para postar então fique ligado e até o próximo post.
