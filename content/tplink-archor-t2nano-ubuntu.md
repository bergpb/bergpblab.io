Title: Instalando drivers para o adaptador TP-Link AC600 Archer T2U Nano
Date: 2022-07-20 09:00
Modified: 2022-07-20 09:00
Category: Drivers
Tags: drivers, linux, wifi
Slug: tplink-archert2
Authors: Berg Paulo
Status: published


Recentemente troquei o plano do provedor de internet onde moro, anteriormente haviam apenas planos com velocidade até 100Mbps disponível para a minha localidade,
mas recentemente o provedor adicionou novos planos e decidi por realizar um upgrade, agora temos até 400Mpbs de velocidade disponível.

Após a instalação da mesma, percebi que a placa wifi do meu notebook (Dell Inspiron 5000), só aceita até 100Mbps de velocidade, e o mesmo para a porta Ethernet,
e para um modelo que foi comprado em 2018 não era de se esperar muito.

Resolvi investir em um adaptador usb wifi para que possa aproveitar o máximo da nova velocidade, e depois de algumas pesquisas acabei optando pelo modelo,
TP-Link AC600 Archer T2U Nano, que me custou cerca de R$ 60 na época.

Encomenda recebida agora chegou a hora de testar a nova velocidade no notebook, conectei o dispositivo ao meu notebook e para minha surpresa nada aconteceu.

Depois de pesquisar um pouco na internet vi que seria necessário realizar a instalação dos drivers desse dispositivo junto ao kernel do linux, e vou descrever como fiz esses passos:

1. Instalar as dependências necessárias para build e instalação dos drivers:

    `$ sudo apt install dkms git build-essential libelf-dev linux-headers-$(uname -r)`

2. Clonar o repositório rtl8812au com o git:

    `$ git clone https://github.com/aircrack-ng/rtl8812au.git`

3. Entrar na pasta do repositório clonado anteriormente:

    `$ cd rtl8812au`

4. Instalar o driver:

    `$ sudo make dkms_install`

Após alguns segundos ou minutos dependendo da velocidade de processamento do seu computador, o driver será instalado
e podemos verificar com o seguinte comando:

    :::bash
    $ sudo dkms status
    > 8812au/5.6.4.2_35491.20191025, 5.18.10-76051810-generic, x86_64: installed

Se o adaptador estiver plugado ao usb do computador, basta desconectar e conectar novamente e o mesmo já estará pronto para uso.

Obs: Caso o kernel seja atualizado, provavelmente o driver deverá ser atualizado. Para isso é necessário remover e repetir os passos de instalação.

Para remover basta executar o seguinte comando (atente-se a versão atual do pacote, o mesmo pode ser verificado com o comando `sudo dkms status`):

    :::bash
    $ sudo dkms remove 8812au/5.6.4.2_35491.20191025 --all
